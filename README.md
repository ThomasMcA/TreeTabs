The original TreeTabs (TT) was abandoned for over a year, so I cloned it into TreeTabs2 (TT2).

After spending hundreds of hours on TT2, the original author of TT reappeared. He had already modified his local version of TT, so most of my TT2 changes are now moot.

The license for TT does not allow collaboration, so I can no longer develop TT2. Although I thought the original author was going to modify that license, I am tired of wating for that to happen.

Since the author of TT is too unresponsive for my tastes, I will be developing my own TreeTabsNew. I will post a link to my blog when that's ready.